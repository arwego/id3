#!/usr/bin/env python

import idtree, checktree, graph, tester
import os

def main():
    # Read File
    threshold_numerical_value = True
    print("Threhold: {}".format(threshold_numerical_value))
    path = os.getcwd() + '/csv/'
    train_filename = 'Training50_Modified.csv'
    output_filename = 'german_credit_data_graph'
    output_filename_threshold = 'german_credit_data_graph_threshold'
    target = 'Creditability'
    index_with_numerical_value = [1, 4, 12]
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', \
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', \
                'u']
    file = open(path + train_filename)
    data = []

    if threshold_numerical_value == True:
        for row, line in enumerate(file):
            line = line.strip('\r\n')
            line = line.split(',')
            temp = []
            for col, content in enumerate(line):
                content = content.strip('"')
                if row != 0:
                    if col in index_with_numerical_value:
                        value = ""
                        score = int(content)
                        # 1, Duration of Credit month, value =
                        # ['b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', 'b10']
                        if col == 1:
                            if score > 54:
                                value = alphabet[col] + '1'
                            elif score > 48 and score <= 54:
                                value = alphabet[col] + '2'
                            elif score > 42 and score <= 48:
                                value = alphabet[col] + '3'
                            elif score > 36 and score <= 42:
                                value = alphabet[col] + '4'
                            elif score > 30 and score <= 36:
                                value = alphabet[col] + '5'
                            elif score > 24 and score <= 30:
                                value = alphabet[col] + '6'
                            elif score > 18 and score <= 24:
                                value = alphabet[col] + '7'
                            elif score > 12 and score <= 18:
                                value = alphabet[col] + '8'
                            elif score > 6 and score <= 12:
                                value = alphabet[col] + '9'
                            elif score <= 6:
                                value = alphabet[col] + '10'

                        # 4, Credit Amount, value =
                        # ['e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'e10']
                        elif col == 4:
                            if score > 20000:
                                value = alphabet[col] + '1'
                            elif score > 15000 and score <= 20000:
                                value = alphabet[col] + '2'
                            elif score > 10000 and score <= 15000:
                                value = alphabet[col] + '3'
                            elif score > 7500 and score <= 10000:
                                value = alphabet[col] + '4'
                            elif score > 5000 and score <= 7500:
                                value = alphabet[col] + '5'
                            elif score > 2500 and score <= 5000:
                                value = alphabet[col] + '6'
                            elif score > 1500 and score <= 2500:
                                value = alphabet[col] + '7'
                            elif score > 1000 and score <= 1500:
                                value = alphabet[col] + '8'
                            elif score > 500 and score <= 1000:
                                value = alphabet[col] + '9'
                            elif score <= 500:
                                value = alphabet[col] + '10'

                        # 12, Age years, value = ['m1', 'm2', 'm3', 'm4', 'm5']
                        elif col == 12:
                            if score >= 65:
                                value = alphabet[col] + '5'
                            elif score >= 60 and score <= 64:
                                value = alphabet[col] + '4'
                            elif score >= 40 and score <= 59:
                                value = alphabet[col] + '3'
                            elif score >= 26 and score <= 39:
                                value = alphabet[col] + '2'
                            elif score >= 0 and score <= 25:
                                value = alphabet[col] + '1'

                        temp.append(value)
                    else:
                        content = alphabet[col] + str(content)
                        temp.append(content)
                else:
                    temp.append(content)
            data.append(temp)
    else:
        for row, line in enumerate(file):
            line = line.strip('\r\n')
            line = line.split(',')
            temp = []
            for col, content in enumerate(line):
                content = content.strip('"')
                if row != 0:
                    content = alphabet[col] + str(content)
                temp.append(content)
            data.append(temp)

    # Read Attributes
    attributes = data[0]
    data.remove(attributes)

    # Create ID3
    tree = idtree.makeTree(data, attributes, target, 0)
    print("Generated decision tree: Success")
    # print("Tree = %s\n" % str(tree))

    # Draw Tree Graph
    if threshold_numerical_value == False:
        print("Graph name: {}".format('img/' + output_filename))
        graph.create_graph(tree, output_filename, 0)
    else:
        print("Graph name: {}".format('img/' + output_filename_threshold))
        graph.create_graph(tree, output_filename_threshold, 1)

    # Compare Tree Graph with Stored Tree
    # checktree.checkTree(tree)

    # Run Test
    # tester.run_test(tree, attributes, threshold_numerical_value)

if __name__ == '__main__':
    main()

#!/usr/bin/env python

import functools
import graphviz as gv
import collections

def dict_path(path, my_dict, result):
    for k,v in my_dict.iteritems():
        if isinstance(v,dict):
            dict_path(path+"_"+k, v, result)
        else:
            result.append(path+"_"+k + "_"+k+"-"+v)
    return result


def add_nodes(graph, nodes):
    for n in nodes:
        if isinstance(n, tuple):
            graph.node(n[0], **n[1])
        else:
            graph.node(n)
    return graph


def add_edges(graph, edges):
    for e in edges:
        if isinstance(e[0], tuple):
            graph.edge(*e[0], **e[1])
        else:
            graph.edge(*e)
    return graph


def apply_styles(graph):
    styles = {
        'graph': {
            'label': 'ID3: German Credit Data',
            'fontsize': '16',
            'fontcolor': 'white',
            'bgcolor': '#333333',
            'rankdir': 'TB',
        },
        'nodes': {
            'fontname': 'Helvetica',
            'shape': 'hexagon',
            'fontcolor': 'white',
            'color': 'white',
            'style': 'filled',
            'fillcolor': '#006699',
        },
        'edges': {
            'style': 'solid',
            'color': 'white',
            'arrowhead': 'open',
            'fontname': 'Courier',
            'fontsize': '12',
            'fontcolor': 'white',
        }
    }
    graph.graph_attr.update(
        ('graph' in styles and styles['graph']) or {}
    )
    graph.node_attr.update(
        ('nodes' in styles and styles['nodes']) or {}
    )
    graph.edge_attr.update(
        ('edges' in styles and styles['edges']) or {}
    )
    return graph


def create_node_and_path(result):
    s1 = []
    s2 = []
    path = []
    nodes = []

    # print("All path: ")
    for r in result:
        # print r
        for j in range(len(r)-1):
            status = True
            for z in range(len(s1)):
                if r[j] == s1[z] and r[j+1] == s2[z]:
                    status = False
            if status == True:
                # print("Append")
                # print r[j], r[j+1]
                # print ''
                s1.append(r[j])
                s2.append(r[j+1])
        for c in r:
            nodes.append(c)

    nodes = sorted(set(nodes))
    # print("All nodes: ")
    # print nodes

    for z in range(len(s1)):
        temp = []
        temp.append(s1[z])
        temp.append(s2[z])
        path.append(tuple(temp))
        # print temp

    # print("Created path: ")
    # print path
    return nodes, path


def create_graph(tree, filename, form):
    if form == 0:
        graph = functools.partial(gv.Graph, format='svg')
        digraph = functools.partial(gv.Digraph, format='svg')
    elif form == 1:
        graph = functools.partial(gv.Graph, format='png')
        digraph = functools.partial(gv.Digraph, format='png')

    empty = []
    temp_path = []
    result = dict_path("", tree, empty)

    for a in result:
        temp = []
        full = a.split('_')
        full.pop(0)
        for b in full:
            temp.append(b)
        temp_path.append(tuple(temp))

    nodes, path = create_node_and_path(temp_path)
    g = add_edges(add_nodes(digraph(), nodes), path)
    g.render('img/' + filename)


def main():
    Tree = {'Account.Balance': {'a3': {'Type.of.apartment': {'o1': 'u1', 'o2': 'u1', 'o3': 'u1'}}, 'a1': {'Type.of.apartment': {'o1': 'u1', 'o2': 'u1', 'o3': 'u0'}}, 'a2': {'Type.of.apartment': {'o2': 'u1', 'o3': 'u0', 'o1': 'u1'}}}}
    tree = {'a' : {'b' : {'c' : '1', 'd' : '2'}, 'e' : '3', 'd': '2'}}
    no_threshold_tree = {'Account.Balance': {'a3': {'Duration.of.Credit..month.': {'b6': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d3': 'u1', 'd4': 'u1', 'd2': {'Credit.Amount': {'e2978': 'u1', 'e2116': 'u1', 'e1543': 'u1', 'e4611': 'u0'}}, 'd1': 'u1'}}, 'c3': 'u1'}}, 'b36': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d4': {'Credit.Amount': {'e7980': 'u0', 'e7855': 'u0', 'e9572': 'u0', 'e3535': 'u1'}}, 'd3': 'u1', 'd1': 'u1'}}, 'c2': {'Purpose': {'d4': 'u1', 'd3': {'Credit.Amount': {'e2394': 'u1', 'e2299': 'u1', 'e5848': 'u1', 'e4210': 'u0', 'e4473': 'u1', 'e3595': 'u1'}}, 'd1': 'u1'}}, 'c1': 'u1'}}, 'b24': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d2': 'u1', 'd1': 'u1', 'd4': {'Credit.Amount': {'e8648': 'u0', 'e1287': 'u1', 'e2463': 'u1', 'e1940': 'u1', 'e4526': 'u1', 'e1275': 'u1', 'e1344': 'u0', 'e2978': 'u1', 'e2022': 'u1', 'e717': 'u1', 'e4139': 'u1'}}, 'd3': 'u1'}}, 'c2': {'Purpose': {'d2': 'u1', 'd1': 'u1', 'd3': {'Credit.Amount': {'e3235': 'u1', 'e1376': 'u1', 'e1552': 'u1', 'e999': 'u1', 'e1278': 'u1', 'e5943': 'u0', 'e1377': 'u1', 'e1311': 'u1', 'e3105': 'u1', 'e1258': 'u1', 'e3430': 'u1', 'e1413': 'u1', 'e1901': 'u1'}}, 'd4': {'Credit.Amount': {'e1469': 'u1', 'e937': 'u1', 'e947': 'u0', 'e1258': 'u1', 'e4591': 'u0', 'e1393': 'u1', 'e3757': 'u1', 'e7393': 'u1', 'e1249': 'u1'}}}}}}, 'b15': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': {'Purpose': {'d2': 'u1', 'd3': {'Credit.Amount': {'e1386': 'u1', 'e2327': 'u0', 'e1905': 'u1'}}, 'd1': 'u1', 'd4': 'u1'}}}}, 'b12': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d3': {'Credit.Amount': {'e1262': 'u1', 'e719': 'u0', 'e1493': 'u1', 'e2171': 'u1', 'e2279': 'u1', 'e3399': 'u1', 'e2141': 'u1', 'e886': 'u1'}}, 'd4': {'Credit.Amount': {'e3527': 'u1', 'e1386': 'u0', 'e2859': 'u1', 'e1884': 'u1', 'e1330': 'u1'}}, 'd2': {'Credit.Amount': {'e1123': 'u0', 'e1474': 'u1', 'e1424': 'u1', 'e1736': 'u1', 'e763': 'u1'}}}}, 'c3': {'Purpose': {'d4': {'Credit.Amount': {'e2292': 'u0', 'e1185': 'u1', 'e926': 'u1', 'e2247': 'u1', 'e682': 'u1'}}, 'd2': 'u1', 'd3': {'Credit.Amount': {'e930': 'u1', 'e797': 'u0', 'e1555': 'u0', 'e2748': 'u1', 'e996': 'u1', 'e717': 'u1', 'e2012': 'u1', 'e1240': 'u1', 'e701': 'u1'}}, 'd1': 'u1'}}}}, 'b20': 'u1', 'b18': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d3': {'Credit.Amount': {'e1800': 'u1', 'e2238': 'u1', 'e2404': 'u1', 'e1582': 'u1', 'e1864': 'u0', 'e1098': 'u1', 'e1149': 'u1'}}, 'd4': {'Credit.Amount': {'e2169': 'u0', 'e2775': 'u0', 'e1055': 'u1', 'e1028': 'u1'}}, 'd1': 'u1'}}, 'c2': {'Purpose': {'d3': {'Credit.Amount': {'e1453': 'u1', 'e2100': 'u0', 'e4594': 'u1', 'e1473': 'u1'}}, 'd2': {'Credit.Amount': {'e2864': 'u0', 'e1984': 'u1', 'e2515': 'u1', 'e1533': 'u0'}}, 'd4': 'u1'}}}}, 'b30': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d3': 'u1', 'd4': {'Credit.Amount': {'e4272': 'u1', 'e1908': 'u0'}}}}, 'c2': 'u1'}}, 'b21': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d4': 'u0', 'd1': 'u1', 'd2': 'u1'}}, 'c2': 'u1'}}, 'b9': {'Payment.Status.of.Previous.Credit': {'c1': 'u0', 'c3': 'u1', 'c2': {'Purpose': {'d3': {'Credit.Amount': {'e2697': 'u1', 'e745': 'u0'}}, 'd4': 'u1', 'd2': {'Credit.Amount': {'e1980': 'u0', 'e1388': 'u1', 'e1313': 'u1'}}}}}}, 'b4': 'u1', 'b48': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c1': 'u1', 'c2': 'u0'}}, 'b42': 'u1', 'b33': 'u1', 'b11': 'u1', 'b10': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d4': {'Credit.Amount': {'e1240': 'u0', 'e1364': 'u1', 'e1309': 'u0', 'e3949': 'u1', 'e1546': 'u1'}}, 'd3': 'u1', 'd1': 'u1', 'd2': 'u1'}}}}, 'b39': 'u1', 'b60': 'u1', 'b28': 'u1', 'b27': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': 'u0'}}, 'b22': 'u1', 'b13': 'u1', 'b7': 'u1'}}, 'a1': {'Duration.of.Credit..month.': {'b15': {'Payment.Status.of.Previous.Credit': {'c1': 'u0', 'c2': 'u1', 'c3': 'u1'}}, 'b42': 'u0', 'b24': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d4': 'u0', 'd2': 'u1', 'd3': {'Credit.Amount': {'e2384': 'u1', 'e2439': 'u0'}}, 'd1': 'u1'}}, 'c1': {'Purpose': {'d2': {'Credit.Amount': {'e3552': 'u0', 'e2828': 'u1', 'e3349': 'u0'}}, 'd1': 'u1', 'd4': {'Credit.Amount': {'e1193': 'u0', 'e2325': 'u1', 'e1358': 'u0', 'e3161': 'u0'}}, 'd3': 'u0'}}, 'c3': {'Purpose': {'d4': {'Credit.Amount': {'e1382': 'u1', 'e4870': 'u0'}}, 'd3': 'u1', 'd1': 'u1'}}}}, 'b9': 'u1', 'b6': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': {'Purpose': {'d3': 'u1', 'd2': 'u1', 'd4': {'Credit.Amount': {'e1374': 'u1', 'e14896': 'u0', 'e1203': 'u1'}}}}}}, 'b11': 'u1', 'b10': 'u1', 'b30': 'u1', 'b12': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d3': {'Credit.Amount': {'e701': 'u1', 'e1107': 'u1', 'e674': 'u0', 'e1200': 'u1'}}, 'd2': 'u1', 'd4': {'Credit.Amount': {'e759': 'u0', 'e902': 'u0', 'e900': 'u0', 'e1274': 'u0', 'e1893': 'u1', 'e1228': 'u0'}}}}, 'c3': {'Purpose': {'d3': 'u1', 'd1': 'u1', 'd4': 'u1', 'd2': 'u0'}}, 'c1': {'Purpose': {'d4': {'Credit.Amount': {'e339': 'u1', 'e697': 'u0'}}, 'd3': 'u0'}}}}, 'b18': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': {'Purpose': {'d4': {'Credit.Amount': {'e4380': 'u1', 'e1216': 'u0', 'e2249': 'u1'}}, 'd2': {'Credit.Amount': {'e3650': 'u1', 'e2039': 'u0'}}, 'd1': 'u0', 'd3': {'Credit.Amount': {'e1345': 'u0', 'e1936': 'u1', 'e1217': 'u0'}}}}, 'c1': 'u0'}}, 'b48': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d3': {'Credit.Amount': {'e6758': 'u0', 'e7476': 'u1'}}, 'd4': 'u0', 'd1': 'u1'}}, 'c1': 'u0', 'c3': 'u0'}}, 'b45': 'u0', 'b47': 'u1', 'b21': 'u1', 'b60': 'u0', 'b36': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d3': 'u0', 'd4': 'u0', 'd2': 'u1'}}, 'c1': 'u0', 'c2': {'Purpose': {'d2': {'Credit.Amount': {'e5179': 'u0', 'e3620': 'u1'}}, 'd1': 'u0', 'd4': 'u0'}}}}, 'b28': 'u0', 'b8': 'u1', 'b20': {'Payment.Status.of.Previous.Credit': {'c2': 'u1', 'c3': 'u0'}}, 'b16': 'u0', 'b40': 'u0', 'b33': 'u0', 'b14': 'u0'}}, 'a2': {'Duration.of.Credit..month.': {'b12': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d4': {'Credit.Amount': {'e888': 'u0', 'e1223': 'u0', 'e2002': 'u1', 'e1318': 'u1', 'e7472': 'u1', 'e754': 'u1', 'e841': 'u1', 'e1037': 'u1'}}, 'd3': {'Credit.Amount': {'e639': 'u0', 'e625': 'u1', 'e1158': 'u1', 'e1484': 'u0', 'e2930': 'u1', 'e6468': 'u0', 'e1331': 'u0', 'e1103': 'u1', 'e766': 'u0'}}, 'd2': 'u1'}}, 'c3': 'u1', 'c1': 'u1'}}, 'b60': {'Payment.Status.of.Previous.Credit': {'c2': 'u0', 'c3': 'u1'}}, 'b18': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d2': {'Credit.Amount': {'e7374': 'u1', 'e3612': 'u1', 'e1928': 'u0', 'e4297': 'u0'}}, 'd3': 'u1', 'd4': 'u1'}}, 'c2': {'Purpose': {'d3': 'u1', 'd2': 'u0', 'd4': 'u1'}}}}, 'b8': 'u1', 'b9': {'Payment.Status.of.Previous.Credit': {'c1': 'u0', 'c3': {'Purpose': {'d3': {'Credit.Amount': {'e1501': 'u0', 'e1154': 'u1'}}, 'd2': 'u1'}}, 'c2': {'Purpose': {'d3': {'Credit.Amount': {'e1199': 'u1', 'e1670': 'u0'}}, 'd4': 'u1', 'd2': {'Credit.Amount': {'e959': 'u0', 'e2030': 'u1'}}}}}}, 'b36': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d3': {'Credit.Amount': {'e2323': 'u1', 'e3711': 'u1', 'e2671': 'u0', 'e2273': 'u1', 'e12612': 'u0'}}, 'd1': {'Credit.Amount': {'e6948': 'u1', 'e9398': 'u0'}}, 'd2': 'u0', 'd4': 'u0'}}, 'c1': 'u0', 'c3': {'Purpose': {'d4': {'Credit.Amount': {'e2862': 'u1', 'e2225': 'u0', 'e7432': 'u1', 'e4455': 'u0', 'e2820': 'u0'}}}}}}, 'b6': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': {'Purpose': {'d3': 'u1', 'd4': 'u0'}}, 'c1': 'u0'}}, 'b21': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d4': 'u0', 'd2': 'u1'}}, 'c3': 'u1'}}, 'b24': {'Payment.Status.of.Previous.Credit': {'c2': {'Purpose': {'d4': {'Credit.Amount': {'e1201': 'u1', 'e1355': 'u0', 'e1246': 'u0'}}, 'd1': 'u0', 'd3': {'Credit.Amount': {'e1967': 'u1', 'e3092': 'u0', 'e2896': 'u1'}}, 'd2': 'u1'}}, 'c1': 'u0', 'c3': {'Purpose': {'d4': {'Credit.Amount': {'e1935': 'u0', 'e2825': 'u1', 'e11938': 'u0', 'e1965': 'u1', 'e6967': 'u1'}}, 'd3': 'u1', 'd2': 'u0'}}}}, 'b30': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d3': 'u0', 'd4': {'Credit.Amount': {'e2181': 'u1', 'e4249': 'u0'}}}}, 'c2': {'Purpose': {'d3': 'u1', 'd2': 'u1', 'd4': 'u0'}}, 'c1': {'Purpose': {'d4': {'Credit.Amount': {'e4221': 'u1', 'e4280': 'u0'}}}}}}, 'b48': {'Payment.Status.of.Previous.Credit': {'c1': {'Purpose': {'d4': {'Credit.Amount': {'e12204': 'u1', 'e3566': 'u1', 'e18424': 'u0', 'e12169': 'u1'}}}}, 'c3': {'Purpose': {'d4': 'u1', 'd2': 'u0', 'd3': 'u0'}}, 'c2': 'u0'}}, 'b42': 'u1', 'b39': 'u1', 'b15': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d4': 'u1', 'd3': 'u0'}}, 'c1': 'u0', 'c2': {'Purpose': {'d4': 'u0', 'd3': 'u1'}}}}, 'b11': 'u1', 'b7': 'u1', 'b45': {'Payment.Status.of.Previous.Credit': {'c3': 'u1', 'c2': 'u0'}}, 'b27': {'Payment.Status.of.Previous.Credit': {'c3': {'Purpose': {'d3': 'u0', 'd1': 'u1'}}}}, 'b10': 'u1', 'b26': 'u1', 'b14': 'u1'}}}}

    graph = functools.partial(gv.Graph, format='png')
    digraph = functools.partial(gv.Digraph, format='png')
    empty = []
    temp_path = []
    result = dict_path("", tree, empty)
    for a in result:
        temp = []
        full = a.split('_')
        full.pop(0)
        for b in full:
            temp.append(b)
        temp_path.append(tuple(temp))

    nodes, path = create_node_and_path(temp_path)
    g = add_edges(add_nodes(digraph(), nodes), path)
    g = apply_styles(g)
    g.render('img/g2')

    # create_graph(Tree, 'g2')

if __name__ == '__main__':
    main()
